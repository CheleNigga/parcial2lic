// DOM
// CAJAS
const form1 = document.getElementById("form1")
const form2 = document.getElementById("form2")
const errorState = document.getElementById("error")
const salida = document.getElementById("salida")

// SINGLE ELEMENTS
const nombre = document.getElementById("nombre")
const apellido = document.getElementById("apellido")
const codigo = document.getElementById("codigo")
const nota = document.getElementById("nota")
const uv = document.getElementById("uv")
const errorMessage = document.getElementById("errorMessage")
const txtSalida = document.getElementById("txtSalida")
const materia = document.getElementById("materia")
//  BOTONES
const btnDatos = document.getElementById("btnDatos")
const btnMateria = document.getElementById("btnMateria")

// VARIABLES
let notas = []
let uvs = []
// Eventos
btnDatos.addEventListener("click", () => {ValidacionDeDatos();})
btnMateria.addEventListener("click", () =>{
    validarMaterias()
})
document.addEventListener("keydown",(e) => {
    if (e.code === "Enter"){
        if (form1.classList.contains("cajaActual")){
            ValidacionDeDatos()
        } else if (form2.classList.contains("cajaActual")){
            validarMaterias()
        }
    }
})

// FUNCIONES
const ValidacionDeDatos = () =>{
    if (validarTxt("nombre")){
        if (validarTxt("apellido")){
            // if (validarCodigo()){
            // }
                form1.classList.toggle("oculto")
                form1.classList.toggle("cajaActual")
                form2.classList.toggle("oculto")
                form2.classList.toggle("cajaActual")
                var alumno = new Alumno(nombre.value,apellido.value,codigo.value,0,0,0)
        }else{
            apellido.focus()
        }
    }else{
        nombre.focus()
    }
}

const validarMaterias = () =>{
    if (notas.length < 7){
        notas.push(nota.value)
        uvs.push(uv.value)
        materia.innerText = notas.length + 1
        nota.value = 0
        uv.value = 0
        nota.focus()
        if (notas.length == 7){
            form2.classList.toggle("oculto")
            form2.classList.toggle("cajaActual")
            salida.classList.toggle("oculto")
            salida.classList.toggle("cajaActual")
            alumno.MediaNotas = alumno.calculoMediaNotas
            alumno.MediaUV = alumno.calculoMediaUV
            alumno.CUM = alumno.caluculocum
            txtSalida.innerText = alumno.mostrarResumen()
        }
    }
}
const validarTxt = (tipo) => {
    if (tipo === "nombre"){
        if (nombre.value.length > 0){
            var re = new RegExp("^[A-Za-z _]*[A-Za-z][A-Za-z _]*$")
            if (re.test(nombre.value)){
                return true;
            }else{
                errorState.classList.toggle("oculto")
                errorMessage.innerText = `Error. Los caracteres de ${tipo} estan incorrectos`
                return false
            }
        }else{
            errorState.classList.toggle("oculto")
            errorMessage.innerText = `Error. No puedes dejar tu ${tipo} en blanco`
            return false
        }
    }else{
            if (apellido.value.length > 0){
                var re = new RegExp("^[A-Za-z _]*[A-Za-z][A-Za-z _]*$")
                if (re.test(apellido.value)){
                    return true;
                }else{
                    errorState.classList.toggle("oculto")
                    errorMessage.innerText = `Error. Los caracteres de ${tipo} estan incorrectos`
                    return false
                }
            }else{
                errorState.classList.toggle("oculto")
                errorMessage.innerText = `Error. No puedes dejar tu ${tipo} en blanco`
                return false
            }
            
    }
}

const validarCodigo = () => {
    if (codigo.value.length > 0){
        var re = new RegExp("/^[A-Z]{2}[0,1,2,9]{1}[0-9]{5}$/")
        console.log(codigo.value);
        if (re.test(codigo.value)){
            return true;
        }else{
            errorState.classList.toggle("oculto")
            errorMessage.innerText = `Error. El código no coincide con el tipo de código de la Universidad`
            return false
        }
    }else{
        errorState.classList.toggle("oculto")
        errorMessage.innerText = `Error. No puedes dejar un tu codigo en blanco`
        return false
    }
}


class Alumno {
    constructor(name, apellido, codigo, MediaNotas, MediaUV, CUM) {
        this.name = name;
        this.apellido = apellido;
        this.codigo = codigo;
        this.MediaNotas = MediaNotas;
        this.MediaUV = MediaUV;
        this.CUM = CUM;
    }
    
    mostrarResumen() {
        return `${this.name} Podras cursar ${this.caluculoUV}`;
    }

    caluculocum = () => {
        return alumno.MediaNotas / alumno.MediaUV
    }
    
    calculoMediaNotas = () => {
        return notas.reduce((a, b) => a + b) / notas.length
    }
    
    calculoMediaUV = () => {
        return uvs.reduce((a, b) => a + b) / uvs.length
    }

    caluculoUV = () => {
        var cum = alumno.CUM
        if (cum >= 7.5){
            return "32 uv."
        } else if (cum >= 7.0){
            return "24 uv."
        } else if (cum > 6){
            return "20 uv."
        } else {
            return "16 uv."
        }
    }
}